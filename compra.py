#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ("patatas", "leche", "pan")

def main():
    seguir = True
    especifica = []

    print("Elemento a comprar: ")
    while seguir:
        i = input()
        if i:
            especifica.append(i)
            print("Elemento a comprar: ")
        else:
            seguir = False

    especifica = tuple(especifica)

    print("Lista de la compra: ")
    for i in habitual + especifica:
        print(i)

    compra_habitual = len(habitual)
    compra_especifica = len(especifica)
    elementos_totales = len(habitual) + compra_especifica

    print("Elementos habituales: ", compra_habitual)
    print("Elementos específicos: ", compra_especifica)
    print("Elementos en lista: ", elementos_totales)
if __name__ == '__main__':
    main()